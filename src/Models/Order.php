<?php

namespace Model;

class Order
{
    /** @var integer $total */
    public $total;

    /**
     * Order constructor.
     * @param $total
     */
    public function __construct($total)
    {
        $this->total = $total;
    }

    /**
     * @description return all orders of the given custumer
     * @param $customerId
     */
    public static function findAllByCustomer($customerId)
    {

    }
}