<?php

namespace Model;

use Database;

class EmployeeType
{
    /** @var  EmployeeType $type */
    private $type;

    /**
     * EmployeeType constructor.
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @description Get the type of given employee
     * @return EmployeeType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @description Set the type of given employee
     * @param $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}