<?php

namespace Model;


class Employee
{
    /** @var  integer $id */
    private $id;

    /** @var  integer $employeeType_id */
    private $employeeType_id;

    /**
     * Employee constructor.
     * @param $type_id
     */
    public function __construct($employeeType_id)
    {
        $this->employeeType_id = $employeeType_id;
    }

    /**
     * @description return type of given employees
     * @return EmployeeType
     */
    public function getType()
    {
        $employeeType = EmployeeType::findById($this->employeeType_id);

        return $employeeType->getType();
    }

    /**
     * @return Customer
     */
    public function getCustomers()
    {
        $customers = Customer::findAllByEmployee($this->id);

        return $customers;
    }

}