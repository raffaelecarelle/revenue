<?php

namespace Model;

class Customer
{
    /** @var  int $id */
    private $id;

    /** @var  string $firstName */
    private $firstName;

    /** @var  string $lastName */
    private $lastName;

    /**
     * Customer constructor.
     * @param $firstName
     * @param $lastName
     */
    public function __construct($firstName, $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    /**
     * @description Get the id of the customer
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @description Get the first name of the custumer
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @descritpion Get the last name of the custumer
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     * @description Set the id of the customer
     * @return integer
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @description Set the first name of the custumer
     * @return string
     */
    public function setFirstName($fisrtName) {
        $this->firstName = $fisrtName;
    }

    /**
     * @descritpion Set the last name of the custumer
     * @return string
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    /**
     * @description find all orders of the customer
     * @return Order
     */
    public function getOrders()
    {
        $orders = Order::findAllByCustomer($this->id);

        return $orders;
    }

}