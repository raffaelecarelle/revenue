<?php
/**
 * Partendo dalla classe Revenue crea le classi mancanti (Employee, EmployeeType, Customer, Order) che implementino
 * almeno i metodi citati. Utilizza gli standard PSR-2 e PSR-4, commenta tutto il codice in inglese utilizzando PHPDoc.
 * Non utilizzare framework.
 * Riscrivi il metodo getTotal della classe Revenue senza utilizzare cicli (for, foreach, while, etc.).
 * Crea un pacchetto con composer utilizzando le classi create.
 *
 * ** BONUS **
 * Implementa almeno un design pattern che ritieni possa essere applicato.
 * Installa un pacchetto composer di tua scelta ed utilizzalo all'interno di una delle classi.
 * Usa Traits, Closures, Magic Methods.
 */

Use Traits\UtilsTrait;
use Intervention\Image\ImageManager;

class Revenue
{
    use UtilsTrait;

    public function getTotal($employees = [])
    {
        $totalRevenue = 0;
        $minSales = 75000;

        $totalRevenue = $this->iterateEmployeesAndCountTotal($employees, 'Sales', $totalRevenue, $minSales);

        return $totalRevenue;
    }

    public function resizeImage($imagePath, $with, $height)
    {
        $manager = new ImageManager(array('driver' => 'imagick'));
        $image = $manager->make($imagePath)->resize($with, $height);

        return $image;
    }
}

