<?php

namespace Traits;


trait UtilsTrait
{
    public function iterateEmployeesAndCountTotal(array $employees, $type, $totalRevenue, $minSales)
    {
        foreach ($employees as $employee) {
            $employeeType = $employee->getType();
            if ($employeeType == $type) {
                $customers = $employee->getCustomers();
                foreach ($customers as $customer) {
                    $customerTotalSales = 0;
                    foreach ($customer->getOrders() as $order) {
                        $customerTotalSales += $order->total;
                    }
                    if ($customerTotalSales >= $minSales) {
                        $totalRevenue += $customerTotalSales;
                    }
                }
            }
        }
        return $totalRevenue;
    }
}